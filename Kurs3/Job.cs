﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs3
{
    class Job
    {
        public string Name { get; set; }

        public Job(string name)
        {
            Name = name;
        }
    }
}
