﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs3
{
    class Employee
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Employee(string name)
        {
            Name = name;
            Age = new Random().Next(40);
        }
    }
}
