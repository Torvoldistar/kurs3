﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs3
{
    class VenAlg
    {
        private int numRows;//число строк
        private int numCols;//число столбцов
        private bool[,] primes;//Массив отметок
        private bool[,] stars;//Массив нулей
        private bool[] rowsCovered;//покрытые строки
        private bool[] colsCovered;//покрытые столбцы
        private double[][] costs;//матрица
        //конструктор
        public VenAlg(double[][] theCosts)
        {
            costs = theCosts;
            numRows = costs.Length;
            numCols = costs[0].Length;

            primes = new bool[numRows, numCols];
            stars = new bool[numRows, numCols];

            // Инициализация массивов с покрытием строк/столбцов
            rowsCovered = new bool[numRows];
            colsCovered = new bool[numCols];
            for (int i = 0; i < numRows; i++)
            {
                rowsCovered[i] = false;
            }

            for (int j = 0; j < numCols; j++)
            {
                colsCovered[j] = false;
            }

            // Инициализация матриц
            for (int i = 0; i < numRows; i++)
            {
                for (int j = 0; j < numCols; j++)
                {
                    primes[i, j] = false;
                    stars[i, j] = false;
                }
            }
        }
        //реализует венгерский алгоритм, находит максимальное паросочетание 
        public int[][] Execute()
        {
            SubtractRowColMins();

            this.FindStars(); 
            this.ResetCovered(); 
            this.CoverStarredZeroCols(); 

            while (!AllColsCovered())
            {
                int[] primedLocation = this.PrimeUncoveredZero();

                if (primedLocation[0] == -1)
                {
                    this.MinUncoveredRowsCols(); 
                    primedLocation = this.PrimeUncoveredZero(); 
                }

                int primedRow = primedLocation[0];
                int starCol = this.FindStarColInRow(primedRow);
                if (starCol != -1)
                {

                    rowsCovered[primedRow] = true;
                    colsCovered[starCol] = false;
                }
                else
                {

                    this.AugmentPathStartingAtPrime(primedLocation);
                    this.ResetCovered();
                    this.ResetPrimes();
                    this.CoverStarredZeroCols();
                }
            }

            return this.StarsToAssignments(); 
        }

        //Составление списка смежности ( нахождение нулей )
        public int[][] StarsToAssignments()
        {
            int[][] toRet = new int[numCols][];
            for (int j = 0; j < numCols; j++)
            {
                toRet[j] = new int[]
                {
                    this.FindStarRowInCol(j), j
                }; 
            }

            return toRet;
        }

        //Сбрасывает текущие отметки
        public void ResetPrimes()
        {
            for (int i = 0; i < numRows; i++)
            {
                for (int j = 0; j < numCols; j++)
                {
                    primes[i, j] = false;
                }
            }
        }

        //Сбрасывает покрытие строк/столбцов
        public void ResetCovered()
        {
            for (int i = 0; i < numRows; i++)
            {
                rowsCovered[i] = false;
            }

            for (int j = 0; j < numCols; j++)
            {
                colsCovered[j] = false;
            }
        }
        //Поиск нулей
        public void FindStars()
        {
            bool[] rowStars = new bool[numRows];
            bool[] colStars = new bool[numCols];

            for (int i = 0; i < numRows; i++)
            {
                rowStars[i] = false;
            }

            for (int j = 0; j < numCols; j++)
            {
                colStars[j] = false;
            }

            for (int j = 0; j < numCols; j++)
            {
                for (int i = 0; i < numRows; i++)
                {
                    if (costs[i][j] == 0 && !rowStars[i] && !colStars[j])
                    {
                        stars[i, j] = true;
                        rowStars[i] = true;
                        colStars[j] = true;
                        break;
                    }
                }
            }
        }
        //Поиск минимального среди непокрытых и его вычитание
        private void MinUncoveredRowsCols()
        {
            double minUncovered = double.MaxValue;
            for (int i = 0; i < numRows; i++)
            {
                if (!rowsCovered[i])
                {
                    for (int j = 0; j < numCols; j++)
                    {
                        if (!colsCovered[j])
                        {
                            if (costs[i][j] < minUncovered)
                            {
                                minUncovered = costs[i][j];
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < numRows; i++)
            {
                if (rowsCovered[i])
                {
                    for (int j = 0; j < numCols; j++)
                    {
                        costs[i][j] = costs[i][j] + minUncovered;
                    }
                }
            }

            for (int j = 0; j < numCols; j++)
            {
                if (!colsCovered[j])
                {
                    for (int i = 0; i < numRows; i++)
                    {
                        costs[i][j] = costs[i][j] - minUncovered;
                    }
                }
            }
        }
        //пометка непокрытого нуля
        private int[] PrimeUncoveredZero()
        {
            int[] location = new int[2];

            for (int i = 0; i < numRows; i++)
            {
                if (!rowsCovered[i])
                {
                    for (int j = 0; j < numCols; j++)
                    {
                        if (!colsCovered[j])
                        {
                            if (costs[i][j] == 0)
                            {
                                primes[i, j] = true;
                                location[0] = i;
                                location[1] = j;
                                return location;
                            }
                        }
                    }
                }
            }

            location[0] = -1;
            location[1] = -1;
            return location;
        }
        //Добавление увеличивающего пути, начинающего в отметке
        private void AugmentPathStartingAtPrime(int[] location)
        {
            List<int[]> primeLocations = new List<int[]>(numRows + numCols);
            List<int[]> starLocations = new List<int[]>(numRows + numCols);
            primeLocations.Add(location);

            int currentRow = location[0];
            int currentCol = location[1];
            while (true)
            {
                int starRow = FindStarRowInCol(currentCol);
                if (starRow == -1)
                {
                    break;
                }

                int[] starLocation = new int[]
                {
                    starRow, currentCol
                };
                starLocations.Add(starLocation);
                currentRow = starRow;

                int primeCol = FindPrimeColInRow(currentRow);
                int[] primeLocation = new int[]
                {
                    currentRow, primeCol
                };
                primeLocations.Add(primeLocation);
                currentCol = primeCol;
            }

            UnStarLocations(starLocations);
            StarLocations(primeLocations);
        }

        //Инициализация листа массивов расположений нулей
        private void StarLocations(List<int[]> locations)
        {
            for (int k = 0; k < locations.Count; k++)
            {
                int[] location = locations[k];
                int row = location[0];
                int col = location[1];
                stars[row, col] = true;
            }
        }
        //Инициализация листа массивов расположения не нулей
        private void UnStarLocations(List<int[]> starLocations)
        {
            for (int k = 0; k < starLocations.Count; k++)
            {
                int[] starLocation = starLocations[k];
                int row = starLocation[0];
                int col = starLocation[1];
                stars[row, col] = false;
            }
        }
        // Поиск покрытого пересекающегося со строкой столбца
        private int FindPrimeColInRow(int theRow)
        {
            for (int j = 0; j < numCols; j++)
            {
                if (primes[theRow, j])
                {
                    return j;
                }
            }

            return -1;
        }
        // Поиск нуля в покрытой пересекающейся со столбцом строке
        public int FindStarRowInCol(int theCol)
        {
            for (int i = 0; i < numRows; i++)
            {
                if (stars[i, theCol])
                {
                    return i;
                }
            }

            return -1;
        }

        // Поиск нуля в покрытом пересекающимся со строкой столбце
        public int FindStarColInRow(int theRow)
        {
            for (int j = 0; j < numCols; j++)
            {
                if (stars[theRow, j])
                {
                    return j;
                }
            }

            return -1;
        }
        //все ли столбцы покрыты
        private bool AllColsCovered()
        {
            for (int j = 0; j < numCols; j++)
            {
                if (!colsCovered[j])
                {
                    return false;
                }
            }

            return true;
        }
        //все ли нули в столбцах покрыты
        private void CoverStarredZeroCols()
        {
            for (int j = 0; j < numCols; j++)
            {
                colsCovered[j] = false;
                for (int i = 0; i < numRows; i++)
                {
                    if (stars[i, j])
                    {
                        colsCovered[j] = true;
                        break; 
                    }
                }
            }
        }
        //нахождение минимального в строке/столбце и его вычитание
        private void SubtractRowColMins()
        {
            for (int i = 0; i < numRows; i++)
            {
                double rowMin = double.MaxValue;
                for (int j = 0; j < numCols; j++)
                {
                    if (costs[i][j] < rowMin)
                    {
                        rowMin = costs[i][j];
                    }
                }

                for (int j = 0; j < numCols; j++)
                {
                    costs[i][j] = costs[i][j] - rowMin;
                }
            }

            for (int j = 0; j < numCols; j++)
            {
                double colMin = double.MaxValue;
                for (int i = 0; i < numRows; i++)
                {
                    if (costs[i][j] < colMin)
                    {
                        colMin = costs[i][j];
                    }
                }

                for (int i = 0; i < numRows; i++)
                {
                    costs[i][j] = costs[i][j] - colMin;
                }
            }
        }
    }
}
