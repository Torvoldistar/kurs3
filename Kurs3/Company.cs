﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs3
{
    class Company
    {
        public List<Employee> Employees { get; set; }
        public List<Job> Jobs { get; set; }
        
        public double[][] WorkPlaces { get; set; }

        public Company(double[][] workPlaces, List<Job> jobs, List<Employee> employees)
        {
            WorkPlaces = workPlaces;
            Jobs = jobs;
            Employees = employees;
        }

        public Dictionary<Employee, Job> Distribute()
        {

            var alg = new VenAlg(WorkPlaces);
            var result = alg.Execute();
            var distribution = new Dictionary<Employee, Job>();
            for (int i = 0; i < result.Length; i++)
            {
                distribution.Add(Employees[result[i][0]], Jobs[result[i][1]]);
            }
            return distribution;
        }
    }
}
