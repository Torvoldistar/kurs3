﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs3
{
    public partial class Form1 : Form
    {
        int n;
        public Form1()
        {
            InitializeComponent();
            dataGridView1.Visible = false;
            button2.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox2.Visible = false;
        }

        //Введите Матрицу
        private void button1_Click(object sender, EventArgs e)
        {
            label1.Visible = false;
            button1.Text = "Подтвердить ввод и пересоздать матрицу";
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();
            dataGridView1.Visible = true;
            button2.Visible = true;
            int.TryParse(textBox1.Text, out n); 
            for (int i = 0; i < n; i++) { dataGridView1.Columns.Add($"{i + 1}", $"{i + 1}"); dataGridView1.AutoResizeColumn(i); }
            for (int i = 0; i < n; i++) { dataGridView1.Rows.Add(); dataGridView1.Rows[i].HeaderCell.Value = $"{i + 1}"; }

        }
        //Венгерский алгоритм
        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            var matrix= new double[n][];
            for (int i = 0; i < n; i++)
            {
                matrix[i] = new double[n];
            }
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    try
                    {
                        matrix[i][j] = int.Parse(dataGridView1[j, i].Value.ToString());
                    }
                    catch
                    {
                        matrix[i][j] = 0; dataGridView1[j, i].Value = 0;
                    }
                }

            var employees = new List<Employee>();
            var jobs = new List<Job>();
            for (int i = 0; i < n; i++)
            {
                employees.Add(new Employee(""));
                jobs.Add(new Job(""));
            }

            var company = new Company(matrix, jobs, employees);
            var distribution = company.Distribute();
            
            double sum = 0;
            //for (int i = 0; i < n; i++) { dataGridView1[result[i][0], result[i][1]].Style.BackColor = Color.Yellow; sum += double.Parse(dataGridView1[result[i][1], result[i][0]].Value.ToString()); }
            foreach (var employee in employees)
            {
                dataGridView1[employees.IndexOf(employee),jobs.IndexOf(distribution[employee])].Style.BackColor = Color.Yellow; sum += double.Parse(dataGridView1[jobs.IndexOf(distribution[employee]), employees.IndexOf(employee)].Value.ToString());
            }
            textBox2.Text = sum.ToString();
        }
        // Контрольный пример
        private void button3_Click(object sender, EventArgs e)
        {
            button1.Text = "Подтвердить ввод и пересоздать матрицу";
            n = 7;
            dataGridView1.Visible = true;
            button2.Visible = true;
            label1.Visible = false;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();
            for (int i = 0; i < n; i++) { dataGridView1.Columns.Add($"{i + 1}", $"{i + 1}"); dataGridView1.AutoResizeColumn(i); }
            dataGridView1.Rows.Add(36,36,47,37,44,41,58);
            dataGridView1.Rows[0].HeaderCell.Value = "1";
            dataGridView1.Rows.Add(28, 47, 43, 42, 49, 49, 60);
            dataGridView1.Rows[1].HeaderCell.Value = "2";
            dataGridView1.Rows.Add(23, 23, 36, 35, 39, 42, 56);
            dataGridView1.Rows[2].HeaderCell.Value = "3";
            dataGridView1.Rows.Add(17, 23, 23, 11, 23, 23, 23);
            dataGridView1.Rows[3].HeaderCell.Value = "4";
            dataGridView1.Rows.Add(23, 35, 34, 27, 48, 48, 62);
            dataGridView1.Rows[4].HeaderCell.Value = "5";
            dataGridView1.Rows.Add(23, 29, 25, 24, 25, 42, 56);
            dataGridView1.Rows[5].HeaderCell.Value = "6";
            dataGridView1.Rows.Add(23, 42, 47, 37, 41, 35, 80);
            dataGridView1.Rows[6].HeaderCell.Value = "7";

        }
    }
}
